import { useEffect, useState } from "react";
import Comp from "./Comp";
import {
  createNewUser,
  getAllUsers,
  deleteAllUsers,
  deleteUserById,
  doAction,
} from "../utils/ApiClient";

export default function Home() {
  // let state = useState([]);
  // let users = state[0];
  // let setUsers = state[1];
  let [users, setUsers] = useState([]);
  let [unstableActs, setUnstableActs] = useState();

  let fetchUsers = async () => {
    // users = await getAllUsers();
    setUsers(await getAllUsers());
    // console.log(users);
  };
  useEffect(() => {
    // console.log("inside useEffect");
    fetchUsers();
    setUnstableActs(localStorage.getItem(`UnstableAction`));
  }, []);

  async function onSumbitFun(event) {
    event.preventDefault();
    let value = event.target[0].value;
    let newUser = await createNewUser({ name: value });
    // let usersCopy = [...users];
    // usersCopy.push(newUser);
    setUsers([...users, newUser]);
  }

  let [name, setName] = useState("Hatem");
  // async function onSumbitFun1(event) {
  //   event.preventDefault();
  //   let newUser = await createNewUser({ name: name });
  //   // let usersCopy = [...users];
  //   // usersCopy.push(newUser);
  //   setUsers([...users, newUser]);
  //   setName("");
  // }

  async function deleteAll() {
    await deleteAllUsers();
    setUsers([]);
  }

  async function deleteUser(id) {
    await deleteUserById(id);
    fetchUsers();
  }

  async function handleError() {
    let flag = await doAction();
    setUnstableActs(`${flag}`);
    localStorage.setItem("UnstableAction", `${flag}`);
  }

  return (
    <div>
      <h4>hello</h4>
      <div>
        {/*TODO // form to create new user */}

        {/* basic html form */}
        {/* <form action="api/users" method="post">
          <label htmlFor="name">Enter Name: </label>
          <input id="name" name="name"></input>
          <button type="submit">submit</button>
        </form> */}

        {/* Uncontrolled method for form */}
        <form action="api/users" method="post" onSubmit={onSumbitFun}>
          <label htmlFor="name">Enter Name: </label>
          <input id="name" name="name" placeholder="Your name"></input>
          <button type="submit">submit</button>
        </form>

        {/* Controlled method for form */}
        {/* <form action="api/users" method="post" onSubmit={onSumbitFun1}>
          <label htmlFor="name">Enter Name: </label>
          <input
            id="name"
            name="name"
            value={name}
            onChange={(event) => {
              setName(event.target.value);
            }}
          ></input>
          {name}
          <button type="submit">submit</button>
        </form> */}
      </div>

      {/* TODO 
            list of users table 
            id - name - delete button 
          */}
      <Comp usersExported={users} setUsers={setUsers}></Comp>

      <div>
        {/*TODO // button to delete all users */}
        <button className="deleteAll" onClick={deleteAll}>
          Delete All
        </button>

        {/*TODO // button to do unstable action */}
        {/* <form action="api/unstable" onSubmit={handleError} method="post">
          <button type="submit">Unstable Action</button>
          <p className="errorText"></p>
        </form> */}
        <button onClick={() => handleError()}>Unstable Action</button>
        <p className="errorText">Current state = {unstableActs}</p>
      </div>
    </div>
  );
}
