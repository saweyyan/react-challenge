import React from "react";
import { deleteUserById, getAllUsers } from "../utils/ApiClient";

export default function Comp(props) {
  // console.log(`this is ${props.children}`);
  // console.log(`Hello from comp`);
  async function deleteUser(id) {
    await deleteUserById(id);
    props.setUsers(await getAllUsers());
  }

  return props.usersExported.map((element) => {
    return (
      <div className="test">
        <p>{element.id}</p>
        <p>{element.name}</p>
        <button onClick={() => deleteUser(element.id)}>Delete</button>
      </div>
    );
  });
}
