import { NextApiRequest, NextApiResponse } from 'next';

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === 'POST') {
    if (Math.random() >= 0.5) {
      return res.status(200).send(true);
    } else {
      return res.status(500).json({
        code: 'internal server error',
        message:
          'مثال علي مشكلة صارت في السيرفر والفرونت اند ليس له علاقة بحلها',
      });
    }
  } else
    return res
      .status(405)
      .json({ error: 'this request method is not implemented' });
}
