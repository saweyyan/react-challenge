import type { NextApiRequest, NextApiResponse } from 'next';
import { usersInMemory } from '../../../utils/data';
import { Users } from '../../../utils/types';

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  // GET: /users/:id
  if (req.method === 'GET') return findOne(req, res);
  // PUT: /users/:id
  else if (req.method === 'PUT') return updateOne(req, res);
  // DELETE: /users/:id
  else if (req.method === 'DELETE') return deleteOne(req, res);
}

const findOne = (req: NextApiRequest, res: NextApiResponse) => {
  const id = req.query.id;
  const userFound = usersInMemory.find((name) => name.id == +id);
  // if user not found
  if (!userFound) {
    return res.status(404).json({ error: `no user with specified id:${id}` });
  } else {
    // return name object to client
    res.status(200).json(userFound);
  }
};
const deleteOne = (req: NextApiRequest, res: NextApiResponse) => {
  const id = req.query.id;
  const userFoundIndex = usersInMemory.findIndex((name) => name.id == +id);
  console.log('usersInMemory', usersInMemory);
  console.log('userFoundIndex', userFoundIndex);
  // if user not found
  if (userFoundIndex === -1) {
    return res.status(404).json({ error: `no user with specified id:${id}` });
  } else {
    usersInMemory.splice(userFoundIndex, 1);
    // return name object to client
    res.status(200).send(true);
  }
};
const updateOne = (req: NextApiRequest, res: NextApiResponse) => {
  const id = req.query.id;
  // take input from client
  const input = req.body;
  // do validation
  if (!input?.name) {
    return res
      .status(400)
      .json({ error: 'please pass "name" param with request body' });
  }

  const userFound = usersInMemory.find((user) => user.id == +id);
  // if user not found
  if (!userFound) {
    return res.status(404).json({ error: `no user with specified id:${id}` });
  } else {
    usersInMemory.forEach((user) => {
      if (user.id === +id) {
        user.name = input.name;
      }
    });
    // return user object with new name to client
    res.status(200).json({ ...userFound, name: input.name });
  }
};
