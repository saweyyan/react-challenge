// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { usersInMemory } from '../../../utils/data';
import { Users } from '../../../utils/types';

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  // GET: /users
  if (req.method === 'GET') return findAll(req, res);
  // POST: /users
  else if (req.method === 'POST') return create(req, res);
  // DELETE: /users
  else if (req.method === 'DELETE') return deleteAll(req, res);
}

const findAll = (req: NextApiRequest, res: NextApiResponse) => {
  // return all names to client
  res.status(200).json(usersInMemory);
};
const create = (req: NextApiRequest, res: NextApiResponse) => {
  // take input from client
  const input = req.body;
  // do validation
  if (!input?.name) {
    return res
      .status(400)
      .json({ error: 'please pass "name" param with request body' });
  }
  const newName: Users = { id: usersInMemory.length + 1, name: input.name };
  // add name to storage
  usersInMemory.push(newName);
  // return created names to client
  res.status(200).json(newName);
};

const deleteAll = (req: NextApiRequest, res: NextApiResponse) => {
  // remove all content of the array
  usersInMemory.splice(0, usersInMemory.length);
  res.status(200).send(true);
};
