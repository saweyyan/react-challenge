export const getAllUsers = () => {
  return fetch("http://localhost:3000/api/users")
    .then((response) => response.json())
    .then((data) => {
      return data;
    });
};

export const deleteAllUsers = () => {
  return fetch("http://localhost:3000/api/users", {
    method: "DELETE",
  })
    .then((response) => response.json())
    .then((data) => {
      return data;
    });
};

export const createNewUser = (name) => {
  return fetch("http://localhost:3000/api/users", {
    method: "POST",
    body: JSON.stringify(name),
    headers: { "Content-Type": "application/json" },
  })
    .then((response) => response.json())
    .then((data) => {
      return data;
    });
};

export const deleteUserById = (id) => {
  return fetch(`http://localhost:3000/api/users/${id}`, {
    method: "DELETE",
  })
    .then((response) => response.json())
    .then((data) => console.log(data));
};

export const updateUserById = () => {
  // TODO write code to do this action using api
};

export const doAction = async () => {
  // TODO write code to do this action using api
  // USING TRY CATCH
  // try {
  //   let response = await fetch("http://localhost:3000/api/unstable", {
  //     method: "POST",
  //   });
  //   const data = await response.json();
  //   console.log(data);
  // } catch (error) {
  //   console.log(error);
  // }
  // using THEN CATCH
  return fetch("http://localhost:3000/api/unstable", {
    method: "POST",
  }).then((response) => {
    return response.ok;
    // if (response.ok) return true;
    // else return false;
  });
  // .then((data) => console.log(data))
  // .catch((err) => console.log("From Catch"));
  // use /unstable endpoint and handle errors
};
